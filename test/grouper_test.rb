require 'minitest/autorun'
require 'faker'
require './lib/grouper.rb'

class GrouperTest < Minitest::Test
  def setup
    @grouper = Grouper.new
  end

  def test_less_or_equal_to_7_groups_number
    names = 6.times.map { Faker::Name.name }
    assert_equal 1, @grouper.group(names).size
  end

  def test_less_or_equal_to_7_groups_size
    names = 6.times.map { Faker::Name.name }
    @grouper.group(names).each do |subgroup|
      assert_equal 6, subgroup.size
    end
  end

  def test_greater_than_and_divisible_by_7
    names = 21.times.map { Faker::Name.name }
    @grouper.group(names).each do |subgroup|
      assert_equal 7, subgroup.size
    end
  end

  def test_greater_than_and_not_divisible_by_7_same_size_groups
    names = 30.times.map { Faker::Name.name }
    @grouper.group(names).each do |subgroup|
      assert_equal 6, subgroup.size
    end
  end

  def test_greater_than_and_not_divisible_by_7_different_size_groups
    names = 13.times.map { Faker::Name.name }
    assert_equal 7, @grouper.group(names).first.size
    assert_equal 6, @grouper.group(names).last.size
  end

end