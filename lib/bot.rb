require 'logging'
require './lib/database'
require './lib/grouper'

class Bot
  def initialize
    @logger = Logging.logger(STDOUT)
    @logger.level = :debug
    @active = false
    @database = Database.new
  end

  def joined(client, channel)
    text = "Hi! It's #{client.self['name']}, type `lunchify help` if you want to know more about me :smile:"
    message(client, channel['id'], text)
    @logger.debug("#{client.self['name']} joined channel #{channel['name']}")
  end

  def send_help(client, channel)
    text = "Hey! It's #{client.self['name']} and I'm here for helping you organise your teams for lunch. \n :one: Just type `lunchify start` to activate me. \n :two: During that time I'll post a message, just type `lunchify me` so I know you want to join us :heart:. \n :three: Finally type `lunchify stop` to deactivate me.\nI'm in charge of:  \n :sparkles: Group you in different teams \n :sparkles: Choose a leader per group who'll be responsible for making reservations"
    message(client, channel, text)
    @logger.debug('Someone asked for help.')
  end

  def start(client, channel)
    if active?
      text = "Hey! I'm already active."
      message(client, channel, text)
      @logger.debug("Someone tried to activate #{client.self['name']}, but it was already activated.")
    else
      @database.reset_users
      text = "Who's joining us for lunch today? :smile: \nJust type `lunchify me` :heart:"
      message(client, channel, text)
      activate
    end
  end

  def stop(client, channel)
    if active?
      deactivate
      users = @database.select_users
      if users
        @logger.debug('Users were retrieved.')
        groups = create_groups(users)
        notify_members(client, channel, groups)
      else
        text = 'Aw it seems like nobody want to go out for lunch today :cry:'
        message(client, channel, text)
      end
    else
      text = "Don't cry for me I'm already dead."
      message(client, channel, text)
      @logger.debug("Someone tried to deactivate #{client.self['name']}, but it was already deactivated.")
    end
  end

  def add_user(client, channel, user)
    if active?
      @database.insert_user(user)
    else
      text = "I'm not active right now! :sweat_smile: Type `wadus help` if you want to know how to use me"
      message(client, channel, text)
    end
  end

  def activate
    @active = true
    @logger.debug("This bot was activated.")
  end

  def deactivate
    @active = false
    @logger.debug("This bot was deactivated.")
  end

  def active?
    @active
  end

  def message(client, channel, text)
    client.typing channel: channel
    client.message channel: channel, text: text
  end
end

def choose_leader(group)
  possible_leaders = group.reject { |member| @database.was_leader_last_week?(member) }
  leader = if possible_leaders == []
             group.sample
           else
             possible_leaders.sample
           end

  @database.insert_leader(leader, Time.now.to_i)
  leader
end

def create_groups(users)
  grouper = Grouper.new
  groups = grouper.group(users)
  @logger.debug('Groups were formed.')

  groups
end

def notify_members(client, channel, groups)
  @database.reset_leaders
  groups.each_with_index do |group, i|
    leader = choose_leader(group)
    text = "<@#{leader}> you are the leader of group #{i + 1} :first_place_medal: !"
    message(client, channel, text)

    group.each do |member|
      text = "<@#{member}> you are a member of group #{i + 1} :robot_face: !"
      message(client, channel, text)
    end
  end
end
