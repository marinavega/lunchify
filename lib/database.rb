require 'sqlite3'

class Database
  def initialize
    @db = SQLite3::Database.new("./data/data.db")
    begin
      @db = SQLite3::Database.open "./data/data.db"
      @db.execute "CREATE TABLE IF NOT EXISTS Users(id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                             user_id STRING UNIQUE)"
      @db.execute "CREATE TABLE IF NOT EXISTS Leaders(id INTEGER PRIMARY KEY AUTOINCREMENT,
                                                             user_id STRING, 
                                                             timestamp STRING)"
    rescue SQLite3::Exception => e
      puts "Exception occurred"
      puts e
    end
  end

  def select_users
    result = @db.execute("SELECT user_id from Users").flatten
    return false if result == []
    result
  end

  def insert_user(user_id)
    @db.execute( "INSERT OR IGNORE INTO Users(user_id) VALUES ('#{user_id}')")
  end

  def insert_leader(user_id, unix_timestamp)
    @db.execute( "INSERT INTO Leaders(user_id, timestamp) VALUES ('#{user_id}', '#{unix_timestamp}')")
  end

  def was_leader_last_week?(user_id)
    timestamp = (Time.now - (60*60*24*8)).to_f
    result = @db.execute("SELECT * from Leaders WHERE user_id = '#{user_id}' AND timestamp > '#{timestamp}'")
    return false if result == []
    true
  end

  def reset_users
    @db.execute("DELETE FROM Users WHERE 1 = 1")
  end

  def reset_leaders
    timestamp = (Time.now - (60*60*24*8)).to_f
    @db.execute("DELETE FROM Leaders WHERE timestamp < '#{timestamp}'")
  end
end
