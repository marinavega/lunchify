class Grouper
  MAX = 7

  def group(users)
    return [users] if users.size <= MAX
    return users.each_slice(MAX).to_a if users.size % MAX == 0
    group_splitter(users)
  end

  private def group_splitter(users)
    n = MAX
    groups = users.each_slice(MAX).to_a
    return groups if groups.last.size == MAX - 1

    while groups.last.size < n
      index = groups.index { |group| group.size == n }
      unless index
        n = n - 1
        next
      end
      groups.last << groups[index].pop
    end

    groups
  end
end
