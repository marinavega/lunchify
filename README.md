## Lunchify
A simple Slack bot powered by Ruby.

### To run the bot:
`SLACK_TOKEN='<YOUR-TOKEN>' bundle exec ruby start.rb`

### To run the tests:
`ruby test/grouper_test.rb`

### Slack commands:
*Activation*: `lunchify start`
*Join a group for lunch*: `lunchify me`
*Deactivation*: `lunchify stop`
*Ask for help*: `lunchify help`