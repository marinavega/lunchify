require 'slack-ruby-client'
require './lib/bot'

Slack.configure do |config|
  config.token = ENV['SLACK_TOKEN']
  raise 'Missing config token' unless config.token
end

client = Slack::RealTime::Client.new
bot = Bot.new

client.on :channel_joined do |data|
  bot.joined(client, data['channel'])
end

client.on :message do |data|
  case data['text']
  when 'lunchify help' then
    bot.send_help(client, data['channel'])
  when 'lunchify start' then
    bot.start(client, data['channel'])
  when 'lunchify me' then
    bot.add_user(client, data['channel'], data['user'])
  when 'lunchify stop' then
    bot.stop(client, data['channel'])
  end
end

client.start!
